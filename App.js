import React, { Component } from "react";
import { View, Button, Text } from "react-native";
import { LoginButton, AccessToken } from "react-native-fbsdk";
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes
} from "react-native-google-signin";

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user_facebook: [],
      uid_facebook: "",
      isSigninInProgress: false,
      user_google: [],
      userInfo: [],
      uid_google: "",
      email: ""
    };
  }

  async componentDidMount() {
    //FACEBOOK
    this._userFace();
    //GOOGLE
    this._configureGoogleSignIn();
    await this._getCurrentUserInfo();
    // console.log(this.user_facebook);
  }

  //////////////GOOGLE//////////////////////

  _configureGoogleSignIn() {
    GoogleSignin.configure({
      // scopes: ["https://www.googleapis.com/auth/drive.readonly"],
      webClientId:
        "265961694803-rj8pcqesnpgbi89487fmcvpcj3gpvsn0.apps.googleusercontent.com",
      offlineAccess: true // if you want to access Google API on behalf of the user FROM YOUR SERVER
    });
  }

  _signIn = async () => {
    this._configureGoogleSignIn();

    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      console.log(userInfo);
      this.setState({ userInfo });
      this.setState({ email: userInfo.user.email });
      this.setState({ uid_google: userInfo.user.id });
      this.setState({ user_google: userInfo.user });
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log(error);
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (f.e. sign in) is in progress already
        console.log(error);
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
        console.log(error);
      } else {
        // some other error happened
      }
    }
  };

  _signOut = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      this.setState({ email: null });
      this.setState({ uid_google: null }); // Remember to remove the user from your app's state as well
      this.setState({ user_google: null }); // Remember to remove the user from your app's state as well
    } catch (error) {
      console.error(error);
    }
  };

  _isSignedIn = async () => {
    const isSignedIn = await GoogleSignin.isSignedIn();
    this.setState({ isLoginScreenPresented: !isSignedIn });
    console.log(isSignedIn);
  };

  _getCurrentUserInfo = async () => {
    try {
      const userInfo = await GoogleSignin.signInSilently();
      this.setState({ userInfo });
      this.setState({ email: userInfo.user.email });
      this.setState({ uid_google: userInfo.user.id });
      this.setState({ user_google: userInfo.user });
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_REQUIRED) {
        // user has not signed in yet
      } else {
        // some other error
      }
    }
  };
  //////////////GOOGLE//////////////////////
  render() {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <LoginButton
          publishPermissions={["public_profile"]}
          onLoginFinished={(error, result) => {
            if (error) {
              alert("login has error: " + result.error);
            } else if (result.isCancelled) {
              alert("login is cancelled.");
            } else {
              AccessToken.getCurrentAccessToken().then(data => {
                const { accessToken } = data;
                // console.log(accessToken);
                this._initUserFace(accessToken);
              });
            }
          }}
          onLogoutFinished={this._outUserFace}
        />

        <Text>
          {this.state.uid_facebook} {this.state.user_facebook.name}
        </Text>

        {this.state.uid_google ? (
          <Button onPress={this._signOut} title="Cerrar session" />
        ) : (
          <GoogleSigninButton
            style={{ width: 192, height: 48 }}
            size={GoogleSigninButton.Size.Wide}
            color={GoogleSigninButton.Color.Dark}
            onPress={this._signIn}
            disabled={this.state.isSigninInProgress}
          />
        )}

        <Text>
          {this.state.uid_google || ""}{" "}
          {this.state.user_google ? this.state.user_google.givenName : ""}
        </Text>
      </View>
    );
  }

  _initUserFace = async token => {
    fetch("https://graph.facebook.com/v2.5/me?access_token=" + token)
      .then(response => response.json())
      .then(json => {
        //     console.log(json);
        this.setState({
          uid_facebook: json.id,
          user_facebook: json
        });
      })
      .catch(error => {
        console.log(error);
      });
  };

  _outUserFace = () => {
    alert("logout");
    this.setState({
      uid_facebook: "",
      user_facebook: ""
    });
  };

  _userFace = async () => {
    AccessToken.getCurrentAccessToken().then(data => {
      if (data) {
        const { accessToken } = data;
        // console.log(accessToken);
        this._initUserFace(accessToken);
        ///  this.goToHomePage();
      }
    });
  };
}
